import './modules'


function chart() {
  var randomScalingFactor = function() {
    var min = 10,
    max = 20;
    return Math.floor( Math.random() * (max - min + 1) ) + min;
  };

  var ctx = document.getElementById('cases_chart').getContext('2d');
  var myChart = new Chart(ctx, {
      type: 'line',
      data: {
          labels: ['январь 17', 'май 17', 'декабрь 17'],
          datasets: [{
              label: '# of Votes',
              // data: [0.1, 0.2, 0.3, 1, 2, 3],
              data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
              ],

              backgroundColor: 'rgba(247, 119, 102, 0.7)',
              borderColor: 'rgba(255, 159, 64, 1)',
              borderWidth: 2,
          }]
      },
      options: {
          title: {
              display: false,
              text: 'Посещаемость',
              fontSize: 18,
              fontColor: '#525899',
          },
          tooltips: {
            enabled: false
          },
          elements: {
            line: {
              tension: 0.000001
            }
          },
          legend: {
            display: false
          },
          scales: {
              yAxes: [{
                type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                // display: false,

                position: 'right',
                gridLines: {
                  drawBorder: false,
                  drawOnChartArea: true, // only want the grid lines for one axis to show up
                  drawTicks: false,
                  zeroLineColor: 'rgba(0,0,0,0)'
                },
                  ticks: {
                    fontSize: '0',
                    beginAtZero: true,
                    suggestedMin: 10,
                    suggestedMax: 20
                  }
              }],
              xAxes : [{
                gridLines: {
                  drawOnChartArea: false,
                  drawBorder: false,
                  drawTicks: true,
                  color: 'rgba(0,0,0,0)',
                  zeroLineColor: 'rgba(0,0,0,0)',
                  borderDashOffset: 20
                },
                ticks: {
                  fontColor: "#525899",
                  fontFamily: ' "Circe", sans-serif',
                  fontSize: '13'
                },
              }]
          }
      }
  });
};
chart();

function headerNav() {
  var toggle = $('.header__toggle'),
      nav = $('.header__nav'),
      htmlBody = $('body, html'),
      overlay = $('.page_overlay');

  toggle.on('click', function () {
    $(this).addClass('is-active');
    nav.addClass('header__nav--opened');

    overlay.addClass('is-active');
    htmlBody.addClass('overflow-h');
  });

  $('.header__nav-close').on('click', function () {
    toggle.removeClass('is-active');
    nav.removeClass('header__nav--opened');

    overlay.removeClass('is-active');
    htmlBody.removeClass('overflow-h');
  });

  if ($(window).width() < 1161) {
    $('.navigation__link').on('click', function (e) {
      if ($(this).parents('.navigation__item_parent').length) {
        e.preventDefault();
        $(this).next().slideToggle();
      }
    });
  };
}
headerNav();

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 300) {
        $(".header").addClass("header--scrolled");
    } else {
        $(".header").removeClass("header--scrolled");
    }
});

$('.hero__benefits-pagination').on('click', function () {
  var item = $('.hero__benefits-item');
  item.removeClass('is-active');
  $(this).parent(item).addClass('is-active');
  $('.hero__benefits').addClass('is-animated').delay(800).queue(function(){
    $(this).removeClass("is-animated").dequeue();
  });
});

if ($(window).width() < 1025) {
    $('.hero__benefits').slick({
      arrows: false,
      dots: true
    });
}


$('.cases__list').slick({
  fade: true,
  prevArrow: $('.cases__pagination-prev'),
  nextArrow: $('.cases__pagination-next'),
});


$('.game__list').slick({
  arrows: false,
  fade: true,
  dots: true,
  speed: 500,
});

$('.cases_page__carousel').slick({
  dots: true,
  arrows: false,
  fade: true,
  appendDots: '.cases_page__carousel-dots',
});

$('.article__gallery-list').slick({
  slidesToShow: 3,
  slidesToScroll: 3,
  prevArrow: $('.carousel-arrow--prev'),
  nextArrow: $('.carousel-arrow--next'),
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

if ($(window).width() < 767) {
  $('.carousel_mobile_has_dots').slick({
      dots: true,
      arrows: false
  });
  $('.carousel_mobile_has_arrows').slick({
    prevArrow: $('.carousel-arrow--prev'),
    nextArrow: $('.carousel-arrow--next'),
  });

  $('.team__list').slick({
    centerMode: true,
    centerPadding: '80px',
    prevArrow: $('.team__arrow--prev'),
    nextArrow: $('.team__arrow--next'),
  });

  $('.js_tabs_mob').on('click', function () {
    $(this).parent().toggleClass('tabs--open')
  });
}

var massonryInit = function () {
  $('.blog__grid').masonry({
    itemSelector: '.blog__grid-item',
  });
}
massonryInit()
window.onload = function() {
  massonryInit()
};

$('.modal-link').fancybox({
  smallBtn: false,
  buttons: []
});

$('.tabs-container').tabslet();

$('.tabs-container').on("_after", function() {
  massonryInit()
});

// work link more

$('.work__item-more').on('click', function (e) {
  e.preventDefault();
  var link = $(this);
  $(this).siblings('.work__item-text').slideToggle('slow', function () {
    if($(this).is(':visible')) {
      link.text('Скрыть');
    } else {
      link.text('Подробнее');
    }
  });
});

// get value 



$("#get_value").keyup(function(){
  var value = $(this).val();
  $('#give_value').val(value)
  console.log(value);
});


// animation 

new WOW().init()


// Yandex map

if ($('#map').length) {
  ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
        center: [55.76, 37.64],
        controls: [],
        zoom: 17
    }),

    myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
        hintContent: 'Собственный значок метки',
        balloonContent: 'Это красивая метка'
    }, {
        iconLayout: 'default#image',
        iconImageHref: 'images/map_icon.png',
        iconImageSize: [41, 59],
        iconImageOffset: [-5, -38]
    });

    myMap.geoObjects
      .add(myPlacemark);

    myMap.behaviors.disable('scrollZoom'); 
  });
}


// quiz

function calc() {
  var length = $('.js-tab-trigger').length;

  function calcInner() {
    function progress(id) {
      var percent = (parseInt(id) / length) * 100;
      $('.calc__progress-inner').css({width: percent + '%'});
      $('.calc__progress-steps').text("Шаг " + id + " из " + length);
    };

    $('.js-tab-trigger').click(function(e) {
      e.preventDefault();
      var navId = $(this).attr('data-tab'),
          content = $('.js-tab-content[data-tab="'+ navId +'"]');
       
      
      if(content.find('.calc__card--checked').length > 0) {
        $('.js-tab-trigger.active').removeClass('active');
        $(this).addClass('active');
        $('.js-tab-content.active').removeClass('active');
        content.addClass('active');
        
        progress(navId)
      }
    });

    $('.calc__next').on('click', function (e) {
      e.preventDefault()
      var content = $(this).parents('.js-tab-content'),
          contentId = content.attr('data-tab');

      if(content.find('.calc__card--checked').length > 0) {

        content.removeClass('active');
        content.next().addClass('active');

        $('.js-tab-trigger.active').removeClass('active').next().addClass('active');

        progress(++contentId)
      }
    });
  }
  calcInner();
  
  $(".calc__card input[type='radio']").click(function() {
    $(this).parents('.calc__cards').find('.calc__card').removeClass('calc__card--checked')
    if(this.checked) {
      $(this).parent().addClass('calc__card--checked');
    }
  });

}
calc()

// players bg animation 

function gameBgAnimation() {
  var windowWidth = $(window).width();

  $('.game__item').mousemove(function(event) {

    var moveX = (($(window).width() / 2) - event.pageX) * 0.01;
    var moveY = (($(window).height() / 2) - event.pageY) * 0.01;
    $('.game__item-bg').css('transform', "translate(" + moveX + "px," + moveY + "px)");
  });
};

gameBgAnimation();


